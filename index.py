#coding:utf-8
from datetime import *

print("Phonebook  - by alcapitan")
print("Released : 22 Dec 2021")
print()

# Load database
def loaddata():
    phonebook=[]
    with open("data.txt","r") as file:
        for person in file:
            person=person.replace("\n","")
            phonebook.append(person)
            # Table of strings with persons and theirs infos
    phonebook_tmp = [] # Backup of phonebook
    for person in phonebook:
    	person = person.lower()
    	# Upper turn to lower for correct search in phonebook
    	phonebook_tmp.append(person.split(";"))
    	# Table of table with persons and theirs infos
    phonebook = phonebook_tmp # Save phonebook
    count = 0
    for person in phonebook_tmp:
    	dico = {} # Infos of one person
    	for info in person:
    		info_split = info.split("=") # So, usage "=" forbid
    		info_key = info_split[0] # Dictionnary syntax
    		info_value = info_split[1] # Dictionnary syntax
    		dico[info_key] = info_value # Insert info in dico
    	phonebook_tmp[count] = dico # Replace table for dictionnary
    	count += 1 # Persons maked
    phonebook = phonebook_tmp # Save phonebook
    return phonebook

# Create a backup
def backups_run(reason):
	date = datetime.now()
	datePrint = date.strftime("%d/%m/%Y %H:%M:%S") # To text in logs.txt
	dateLog = date.strftime("%y%m%d-%H%M%S") # To name of backup files
	with open("backup/logs.txt","r") as file: # Know number of lines
		count = 0
		for line in file:
			count += 1
	with open("backup/logs.txt","a") as file:
		file.write(f"[{datePrint}] [{count}] \"{reason}\"\n") # Add log of current backup
	with open("data.txt","r") as file:
		data = file.read() # Copy data.txt
	with open(f"backup/data_{dateLog}.backup","x") as file:
		file.write(data) # Paste data.txt
	print("Sauvegarde faite.")

# Save the phonebook
def save(register,reason):
	phonebook = ""
	for person in register:
		phonebook_tmp = "" # Info of current person
		for key,value in person.items(): # For every info
			phonebook_tmp += f"{key}={value};" # Insert info
		phonebook_tmp = phonebook_tmp[:-1] # Delete ";" for fix bogue
		phonebook_tmp += "\n" # Data person ends
		phonebook += phonebook_tmp # Add person data to phonebook
	backups_run(reason) # Run a backup before edit data.txt
	with open("data.txt","w") as file:
		file.write(phonebook) # Edit data.txt

# Boot
def boot():
    while True:
        choose=menu()

# Menu
def menu():
	while True:
		print("")
		print("MENU OF PHONEBOOK")
		print("0 - Aide")
		print("1 - Ajouter un contact")
		print("2 - Rechercher un contact")
		print("3 - Supprimer un contact")
		print("4 - Modifier un contact")
		print("5 - Importer des contacts")
		print("6 - Sauvegardes")
		print("7 - Paramètres")
		print("Tout autre touche - Quitter le programme")
        
		chose=input("Que voulez vous faire : ")
		print("")
		if chose == "0":
			return "help"
		elif chose == "1":
			return "add"
		elif chose == "2":
			search()
		elif chose == "3":
			remove()
		elif chose == "4":
			return "edit"
		elif chose == "5":
			return "import"
		elif chose == "6":
			backups()
		elif chose == "7":
			settings()
		else:
			close()

# Help


# Add a contact


# Search a contact
def search():
    print("SEARCH A CONTACT")
    request = input("Who are you search : ")
    request = request.lower() # Fix bogue of input
    register = loaddata() # Return phonebook
    list = [] # List of names in phonebook
    for person in register:
    	list.append(person['name'])
    if request in list:
    	localisation = list.index(request) # Where is the request in the phonebook
    	for key,value in register[localisation].items(): # Return the keys and values of the request
    		print(f"{key.capitalize()} : {value}") # Print every keys and values
    else:
    	print("Contact wanted not found")

# Remove a contact
def remove():
	print("REMOVE A CONTACT")
	request = input("Who do you want remove : ")
	request = request.lower() # Fix bogue of input
	register = loaddata() # Return phonebook
	list = [] # List of names in phonebook
	for person in register:
	   list.append(person['name'])
	if request in list:
	   localisation = list.index(request) # Where is the request in the phonebook
	   register.pop(localisation)
	   save(register,f"Remove contact \"{request}\"")
	   print("Removed successly")
	else:
		print("Contact wanted not found")

# Edit a contact
def ecriture_repertoire(repertoire):
    print("Ecriture en cours")
    while True:
        nom=input("Nom de la personne (0 pour quitter): ")

        if nom=="0" or nom==" " or nom=="":
            print("Vous n'avez pas entré un nom \nRetour au menu principal...\n")
            break
        
        elif nom in repertoire:
            print("Cette personne existe déjà \nRetour au menu principal...\n")
            break
        tel=input("Le numéro de téléphone : ")
        
        if tel=="0" or tel==" " or tel=="":
            print("Vous n'avez pas entré un numéro de téléphone \nRetour au menu principal...\n")
            break
        
        with open("data.txt","a")as fichier:   
            fichier.write(nom+"/"+tel+"\n")
            fichier.close()

# Import a contact


# Backups
def backups():
	print("BACKUPS")
	want = True
	while want == True:
		print("0 - Créer une sauvegarde")
		print("1 - Restaurer depuis une sauvegarde")
		print("2 - Supprimer les sauvegardes au delà des 10 les plus récentes")
		print("Tout autre touche - Revenir au menu")
        
		choix=input("Que voulez vous faire : ")
		if choix == "0":
			backups_run("Demande manuelle.")
		elif choix == "1":
			return "restore"
		want = False
		print("")

# Settings
def settings():
	print("SETTINGS")
	want = True
	while want == True:
		print("0 - Langue")
		print("1 - Supprimer toutes les données")
		print("2 - Réparer les fichiers de données")
		print("Tout autre touche - Revenir au menu")
        
		choix=input("Que voulez vous faire : ")
		print("")
		if choix == "0":
			settings_language()
		elif choix == "1":
			return "settings_reset"
		want = False

# Languages
settingsTable = []
with open("settings.txt","r") as file:
	for line in file:
		line = line.replace("\n","")
		settingsTable.append(line)
	languageCurrent = settingsTable[0]
	languageCurrentId = settingsTable[1]
languagesAvailable = ["french","english"]
def settings_language():
	print("CHANGE LANGUAGE(")
	print("Availables languages : ")
	count = 0
	for language in languagesAvailable:
		language = language.capitalize()
		print(f"{count} - {language}")
		count += 1
	want = True
	while want == True:
		userInput = int(input("Quel langue voulez-vous appliquez ?  "))
		if userInput <= len(languagesAvailable):
			languageCurrentId = userInput
			languageCurrent = languagesAvailable[languageCurrentId]
			settingsTable[0] = languageCurrent
			settingsTable[1] = languageCurrentId
			with open("settings.txt","w") as file:
				count = 0
				for element in settingsTable:
					file.write(f"{settingsTable[count]}\n")
					count += 1
			print(f"Langue appliqué : {languagesAvailable[userInput]}")
			want = False
		else:
			print("Erreur !")

# Close the program
def close():
    print("PHONEBOOK ENDS")
    exit()

boot() 